<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="3.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:atom="http://www.w3.org/2005/Atom">
  <xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes"/>
  <xsl:template match="/">
    <html xmlns="http://www.w3.org/1999/xhtml" lang="en">
      <head>
        <title><xsl:value-of select="/atom:feed/atom:title"/></title>
        <meta charset="utf-8"/>
        <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <link rel="stylesheet" href="/css/site.css"/>
        <link rel="stylesheet" href="/css/rss.css"/>
      </head>
      <body>
        <main class="layout-content">
          <div class="py-7">
              <h1 class="flex items-start"><xsl:value-of select="/atom:feed/atom:title"/></h1>
            <p>
              <xsl:value-of select="/atom:feed/atom:subtitle"/>
            </p>

            <h2>Artikuluak</h2>
            <ul>
            <xsl:for-each select="/atom:feed/atom:entry">

                <li>
                  <a>
                    <xsl:attribute name="href">
                      <xsl:value-of select="atom:link"/>
                    </xsl:attribute>
                    <xsl:value-of select="atom:title"/>
                  </a>
                <div class="text-3"><xsl:value-of select="atom:summary"/></div>
              </li>
            </xsl:for-each>
            </ul>
          </div>
        </main>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
