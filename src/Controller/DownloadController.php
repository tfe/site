<?php

namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/download')]
class DownloadController extends AbstractController
{
	public function getIsAdmin()
	{
		return false;
	}
    public function getPath()
    {
        return $this->getParameter('dl_path');
    }

    #[Route('/', name: 'download_root', methods: ['GET'])]
    public function root(Request $request)
    {
        return $this->index($request, '');
    }

    #[Route('/{path}/', requirements: ['path' => '[~a-zA-Z0-9\.\-\_\ \/]*' ], name: 'download_path', methods: ['GET'])]
    public function index(Request $request, $path)
    {
        $start_path = $this->getPath();

        $parent_path = preg_replace("/\/[^\/]+\/+$/","/", $path);
        if($parent_path===$path) {
            $parent_path="";
        }
        if(preg_match("/\/{2,}|\.{2,}/", $path))
        {
            throw $this->createNotFoundException("File not found!");
        }


        if(is_file("$start_path/$path")) {
            $finfo = finfo_open(FILEINFO_MIME);
            $isText= substr(finfo_file($finfo, "$start_path/$path"), 0, 4) == 'text';
            $download= $request->query->get('download');
            if($download || !$isText || filesize("$start_path/$path")>100000)
            {
                return new BinaryFileResponse($start_path."/".$path);
            }
            else
            {
                $data = array(
                    "path" => $path,
                    "parent_path" => $parent_path,
                    "dirs" => array(),
                    "files" => array()
                );
                return $this->render('download/content.html.twig', array_merge($data,[
                    'robots'        => '',
                    'content' => file_get_contents("$start_path/$path"),
                    'filetype' => "language-".preg_replace("/.*\./","", $path),
                    'title'          => 'Fixategiak',
                    'is_admin' 	    => $this->getIsAdmin(),
                    'extra_css'     => ["download.css", 'prism.css'],
                    'extra_js' => ['prism.js'],
                ]));
            }
        }
        if($path && !preg_match("/\/$/",$path))
        {
            $path.="/";
        }

        $data = array(
            "path" => $path,
            "parent_path" => $parent_path,
            "dirs" => array(),
            "files" => array()
        );
        if(!is_dir("$start_path/$path")) {
            throw $this->createNotFoundException("File not found");
        }
        if(!($handle  = opendir("$start_path/$path")))
        {
            throw $this->createNotFoundException('Directory cannot be opened.');
        }
        while($file = readdir($handle))
        {
            if(!preg_match("/^\.+$/", $file))
            {
                if(is_dir("$start_path/$path/$file"))
                {
                    $data['dirs'][] = $file;
                }
                else
                {
                    $data['files'][] = $file;
                }
            }
        }
        sort($data['dirs']);
        sort($data['files']);

        return $this->render('download/index.html.twig', array_merge($data,[
            'robots'        => '',
            'title'          => 'Fixategiak',
            'is_admin' 	    => $this->getIsAdmin(),
            'extra_css'     => ["download.css"],
        ]));
    }
}
