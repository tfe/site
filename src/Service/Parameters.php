<?php

namespace App\Service;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class Parameters extends AbstractController
{
    public function get($name)
    {
        return $this->getParameter($name);
    }
}
